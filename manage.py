from bookmark_new import app, db
from flask.ext.script import Manager, prompt_bool
from models import User
manager = Manager(app)



@manager.command
def initdb():
	db.create_all()
	db.session.add(User(username="Noor", email="noor@synup.com"))
	db.session.add(User(username="Zzzz", email="z@synup.com"))
	db.session.commit()
	print ('Database Intialized')

@manager.command
def dropdb():
	if prompt_bool('Are you sure [y/n]'):
		db.drop_all()


if __name__ =='__main__':
	manager.run()