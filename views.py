import app, db
from flask import render_template, url_for, request, redirect
from form import BookmarkForm
from  flask_login import LoginManager, login_required

login_manager = LoginManager()
login_manager.session_protection ="strong"
login_manager.init_app(app)

def logged_in_user():
	return models.User.query.filter_by(username="Noor").first()

@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html', new_bookmark=models.Bookmark.newest(5))

@app.route('/add', methods=['GET', 'POST'])

def add():
	form = BookmarkForm()
	if form.validate_on_submit():
		url = form.url.data
		description = form.description.data
		bm = models.Bookmark(user=logged_in_user(), url=url, description=description)
		db.session.add(bm)
		db.session.commit()
		return redirect(url_for('index'))


	return render_template('add.html', form=form)

@app.route('/user/<username>')
def user(username):
	user = User.query.filter_by(username=username).first_or_404()
	return render_template('user.html', user = user)


@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

@app.errorhandler(500)
def server_err(e):
	return render_template('500.html'), 500

if __name__ == '__main__':
	app.run(debug=True)