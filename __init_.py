import os
from flask import  Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))


app.config['SECRET_KEY'] = 'kjhksdhfksdhfkjsdhfjkhsdkjfhsdkjhfkjh'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'bookmark.db')

db = SQLAlchemy(app)

import views
import models
